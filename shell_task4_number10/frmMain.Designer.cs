﻿namespace shell_task4_number10
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series22 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series23 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series24 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbMove = new System.Windows.Forms.RadioButton();
            this.rbCompare = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbFourth = new System.Windows.Forms.CheckBox();
            this.cbThird = new System.Windows.Forms.CheckBox();
            this.cbSecond = new System.Windows.Forms.CheckBox();
            this.cbFirst = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.numCount = new System.Windows.Forms.NumericUpDown();
            this.numdN = new System.Windows.Forms.NumericUpDown();
            this.numMaxN = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDrawGraph = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numdN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxN)).BeginInit();
            this.SuspendLayout();
            // 
            // chart
            // 
            chartArea4.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea4.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea4.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea4);
            legend4.Enabled = false;
            legend4.Name = "Legend1";
            this.chart.Legends.Add(legend4);
            this.chart.Location = new System.Drawing.Point(2, 1);
            this.chart.Name = "chart";
            series19.BorderWidth = 3;
            series19.ChartArea = "ChartArea1";
            series19.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series19.Legend = "Legend1";
            series19.Name = "2^k - 1";
            series19.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            series19.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            series20.BorderWidth = 3;
            series20.ChartArea = "ChartArea1";
            series20.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series20.Legend = "Legend1";
            series20.Name = "2^k + 1";
            series20.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            series20.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            series21.BorderWidth = 3;
            series21.ChartArea = "ChartArea1";
            series21.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series21.Legend = "Legend1";
            series21.Name = "(3^k - 1) / 2";
            series21.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            series21.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            series22.BorderWidth = 3;
            series22.ChartArea = "ChartArea1";
            series22.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series22.Legend = "Legend1";
            series22.Name = "2^k - 1 ";
            series22.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            series22.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            series23.BorderWidth = 3;
            series23.ChartArea = "ChartArea1";
            series23.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series23.Legend = "Legend1";
            series23.Name = "2^k + 1 ";
            series23.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            series23.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            series24.BorderWidth = 3;
            series24.ChartArea = "ChartArea1";
            series24.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series24.Legend = "Legend1";
            series24.Name = "(3^k - 1) / 2 ";
            series24.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            series24.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int64;
            this.chart.Series.Add(series19);
            this.chart.Series.Add(series20);
            this.chart.Series.Add(series21);
            this.chart.Series.Add(series22);
            this.chart.Series.Add(series23);
            this.chart.Series.Add(series24);
            this.chart.Size = new System.Drawing.Size(971, 643);
            this.chart.TabIndex = 0;
            this.chart.Text = "chart1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbMove);
            this.panel1.Controls.Add(this.rbCompare);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(979, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(118, 90);
            this.panel1.TabIndex = 1;
            // 
            // rbMove
            // 
            this.rbMove.AutoSize = true;
            this.rbMove.Location = new System.Drawing.Point(15, 58);
            this.rbMove.Name = "rbMove";
            this.rbMove.Size = new System.Drawing.Size(98, 17);
            this.rbMove.TabIndex = 2;
            this.rbMove.Text = "Перемещения";
            this.rbMove.UseVisualStyleBackColor = true;
            // 
            // rbCompare
            // 
            this.rbCompare.AutoSize = true;
            this.rbCompare.Checked = true;
            this.rbCompare.Location = new System.Drawing.Point(15, 35);
            this.rbCompare.Name = "rbCompare";
            this.rbCompare.Size = new System.Drawing.Size(80, 17);
            this.rbCompare.TabIndex = 1;
            this.rbCompare.TabStop = true;
            this.rbCompare.Text = "Сравнения";
            this.rbCompare.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Критерий";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cbFourth);
            this.panel2.Controls.Add(this.cbThird);
            this.panel2.Controls.Add(this.cbSecond);
            this.panel2.Controls.Add(this.cbFirst);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(979, 108);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(118, 190);
            this.panel2.TabIndex = 2;
            // 
            // cbFourth
            // 
            this.cbFourth.AutoSize = true;
            this.cbFourth.Checked = true;
            this.cbFourth.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFourth.Location = new System.Drawing.Point(15, 104);
            this.cbFourth.Name = "cbFourth";
            this.cbFourth.Size = new System.Drawing.Size(103, 17);
            this.cbFourth.TabIndex = 4;
            this.cbFourth.Text = "(2^k - (-1)^k) / 3";
            this.cbFourth.UseVisualStyleBackColor = true;
            this.cbFourth.Visible = false;
            // 
            // cbThird
            // 
            this.cbThird.AutoSize = true;
            this.cbThird.Checked = true;
            this.cbThird.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbThird.Location = new System.Drawing.Point(15, 81);
            this.cbThird.Name = "cbThird";
            this.cbThird.Size = new System.Drawing.Size(82, 17);
            this.cbThird.TabIndex = 3;
            this.cbThird.Text = "(3^k - 1) / 2";
            this.cbThird.UseVisualStyleBackColor = true;
            // 
            // cbSecond
            // 
            this.cbSecond.AutoSize = true;
            this.cbSecond.Checked = true;
            this.cbSecond.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSecond.Location = new System.Drawing.Point(15, 58);
            this.cbSecond.Name = "cbSecond";
            this.cbSecond.Size = new System.Drawing.Size(62, 17);
            this.cbSecond.TabIndex = 2;
            this.cbSecond.Text = "2^k + 1";
            this.cbSecond.UseVisualStyleBackColor = true;
            // 
            // cbFirst
            // 
            this.cbFirst.AutoSize = true;
            this.cbFirst.Checked = true;
            this.cbFirst.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFirst.Location = new System.Drawing.Point(15, 35);
            this.cbFirst.Name = "cbFirst";
            this.cbFirst.Size = new System.Drawing.Size(59, 17);
            this.cbFirst.TabIndex = 1;
            this.cbFirst.Text = "2^k - 1";
            this.cbFirst.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Формулы";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.numCount);
            this.panel3.Controls.Add(this.numdN);
            this.panel3.Controls.Add(this.numMaxN);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(979, 304);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(126, 96);
            this.panel3.TabIndex = 3;
            // 
            // numCount
            // 
            this.numCount.Location = new System.Drawing.Point(44, 64);
            this.numCount.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCount.Name = "numCount";
            this.numCount.Size = new System.Drawing.Size(75, 20);
            this.numCount.TabIndex = 5;
            this.numCount.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numdN
            // 
            this.numdN.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numdN.Location = new System.Drawing.Point(43, 38);
            this.numdN.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numdN.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numdN.Name = "numdN";
            this.numdN.Size = new System.Drawing.Size(75, 20);
            this.numdN.TabIndex = 4;
            this.numdN.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numMaxN
            // 
            this.numMaxN.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numMaxN.Location = new System.Drawing.Point(44, 10);
            this.numMaxN.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numMaxN.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numMaxN.Name = "numMaxN";
            this.numMaxN.Size = new System.Drawing.Size(75, 20);
            this.numMaxN.TabIndex = 3;
            this.numMaxN.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Count";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "dN";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "MaxN";
            // 
            // btnDrawGraph
            // 
            this.btnDrawGraph.Location = new System.Drawing.Point(979, 406);
            this.btnDrawGraph.Name = "btnDrawGraph";
            this.btnDrawGraph.Size = new System.Drawing.Size(125, 23);
            this.btnDrawGraph.TabIndex = 4;
            this.btnDrawGraph.Text = "Построить график";
            this.btnDrawGraph.UseVisualStyleBackColor = true;
            this.btnDrawGraph.Click += new System.EventHandler(this.btnDrawGraph_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 646);
            this.Controls.Add(this.btnDrawGraph);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Graph";
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numdN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxN)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.RadioButton rbMove;
        public System.Windows.Forms.RadioButton rbCompare;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.NumericUpDown numCount;
        public System.Windows.Forms.NumericUpDown numdN;
        public System.Windows.Forms.NumericUpDown numMaxN;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.Button btnDrawGraph;
        public System.Windows.Forms.DataVisualization.Charting.Chart chart;
        public System.Windows.Forms.CheckBox cbFourth;
        public System.Windows.Forms.CheckBox cbThird;
        public System.Windows.Forms.CheckBox cbSecond;
        public System.Windows.Forms.CheckBox cbFirst;
    }
}

