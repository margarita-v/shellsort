﻿namespace shell_task4_number10
{
    public class Info
    {
        int _cntCompare;
        int _cntMove;

        public int cntCompare
        {
            get { return _cntCompare; }
            set { _cntCompare = value; }
        }
        public int cntMove
        {
            get { return _cntMove; }
            set { _cntMove = value; }
        }

        public Info()
        {
            _cntCompare = 0;
            _cntMove = 0;
        }
        public Info(int __cntCompare, int __cntMove)
        {
            _cntCompare = __cntCompare;
            _cntMove = __cntMove;
        }
    }
}
