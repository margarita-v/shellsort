﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace shell_task4_number10
{
    public partial class frmMain : Form
    {
        Elements data;

        public frmMain()
        {
            data = new Elements();
            InitializeComponent();
            Application.Idle += MainIdle;
        }

        private void btnDrawGraph_Click(object sender, EventArgs e)
        {
            Info _info;
            int count = (int)numCount.Value;
            int MaxN = (int)numMaxN.Value;
            int dN = (int)numdN.Value;
            int N = dN;
            chart.Legends[0].Enabled = true;
            chart.ChartAreas[0].AxisX.Minimum = 0;
            for (int i = 0; i < 6; i++)
            {
                chart.Series[i].Points.Clear();
                chart.Series[i].Points.AddXY(0, 0);
            }
            chart.Series[3].Color = Color.DarkBlue;   // 2^k - 1
            chart.Series[4].Color = Color.DarkOrange; // 2^k + 1
            chart.Series[5].Color = Color.DarkRed;    // (3^k - 1) / 2
            while (N <= MaxN)
            {
                for (int i = 0; i < 3; i++)
                {
                    _info = Elements.GetAverage(count, N, i);
                    chart.Series[i].Points.AddXY(N, _info.cntCompare);
                    chart.Series[i + 3].Points.AddXY(N, _info.cntMove);
                }
                N += dN;
            }
            chart.ChartAreas[0].AxisX.Maximum = N - dN;                
        }

        private void MainIdle(object sender, EventArgs e)
        {
            chart.Series[0].Enabled = rbCompare.Checked && cbFirst.Checked;
            chart.Series[1].Enabled = rbCompare.Checked && cbSecond.Checked;
            chart.Series[2].Enabled = rbCompare.Checked && cbThird.Checked;
            chart.Series[3].Enabled = rbMove.Checked && cbFirst.Checked;
            chart.Series[4].Enabled = rbMove.Checked && cbSecond.Checked;
            chart.Series[5].Enabled = rbMove.Checked && cbThird.Checked;
        }
    }
}
