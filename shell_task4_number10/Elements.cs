﻿using System;
using System.Collections.Generic;

namespace shell_task4_number10
{
    public class Elements
    {
        static List<int> mas;
        static Random rand;
        static Info info;
        static Info result;

        public Elements()
        {
            mas = new List<int>();
            rand = new Random();
            info = new Info();
            result = new Info();
        }

        static int GetCurrentN(int h, int formula_num)
        {
            switch (formula_num)
            {
                case 0: // 2^k - 1
                    {
                        return h / 2;
                    }
                case 1: // 2^k + 1
                    {
                        return (h + 1) / 2;
                    }
                case 2: // (3^k - 1) / 2
                    {
                        return h / 3;
                    }
                default: // 2^k - 1
                    {
                        return h / 2;
                    }
            }
        }
        static int GetLastN(int N, int formula_num)
        {
            int i = 2; 
            int pred_res = 1, res = 1;
            switch (formula_num)
            {
                case 0: // 2^k - 1
                    {
                        while (res <= N)
                        {
                            pred_res = res;
                            //res = 2 * pred_res + 1;
                            res = (int)Math.Pow(2, i) - 1;
                            i++;
                        }
                        break;
                    }
                case 1: // 2^k + 1
                    {
                        res = 3;
                        while (res <= N)
                        {
                            pred_res = res;
                            //res = 2 * pred_res - 1;
                            res = (int)Math.Pow(2, i) + 1;
                            i++;
                        }
                        break;
                    }
                case 2: // (3^k - 1) / 2
                    {
                        while (res <= N)
                        {
                            pred_res = res;
                            //res = 3 * pred_res + 1;
                            res = ((int)Math.Pow(3, i) - 1) / 2;
                            i++;
                        }
                        break;
                    }
                default: // 2^k - 1
                    {
                        while (res <= N)
                        {
                            pred_res = res;
                            //res = 2 * pred_res + 1;
                            res = (int)Math.Pow(2, i) - 1;
                            i++;
                        }
                        break;
                    }
            }
            return pred_res;
        }

        public static void Sort(int N, int formula_num)
        {
            mas.Clear();
            for (int i = 0; i < N; i++)
                mas.Add(rand.Next(100));

            info.cntCompare = 0;
            info.cntMove = 0;
            int h = GetLastN(N, formula_num);
            //int index = 0;
            int ind = 0;
            int x;
            bool ok;
            do
            {
                // вычислить текущий шаг
                h = GetCurrentN(h, formula_num);
                //index++;
                // для всех подмассивов текущего шага
                for (int j = 0; j < h; j++)
                {
                    ind = j + h;
                    // выполнить сортировку вставками
                    while (ind < N)
                    {
                        x = mas[ind];
                        info.cntMove++;
                        j = ind - h;
                        ok = false;
                        while (j >= 0 && !ok)
                        {
                            info.cntCompare++;
                            if (mas[j] > x)
                            {
                                mas[j + h] = mas[j];
                                j -= h;
                                info.cntMove++;
                            }
                            else
                                ok = true;
                        }
                        mas[j + h] = x;
                        info.cntMove++;
                        // следующий шаг
                        ind += h;
                    }
                }
            } while (h != 1);
        }
        public static Info GetAverage(int count, int N, int formula_num)
        {
            int sum_cmp = 0, sum_move = 0;
            for (int i = 0; i < count; i++)
            {
                Sort(N, formula_num);
                sum_cmp += info.cntCompare;
                sum_move += info.cntMove;
            }
            sum_cmp /= count;
            sum_move /= count;
            result.cntCompare = sum_cmp;
            result.cntMove = sum_move;
            return result;
        }
    }
}
